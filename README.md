# Mr.J

>一个基于Spring Boot和Vue的Web开发脚手架，整合和最基础的RBAC权限控制，包括：菜单权限、按钮权限、接口权限。

- 前端使用vue-cli，后端使用Spring Boot，两个全家桶强强联合。
- 用简单优雅的方式整合shiro
- 使用Gradle持续构建特性，开发时修改java代码无需重启
- 使用vue-element-admin做前端模板，摆脱写jQuery的痛苦
- 多种灵活形式的前后端分离方式，包括开发阶段的前后端分离和部署的前后端分离

**效果图：**

![p1](_doc/image/preview_1.png)
![p2](_doc/image/preview_2.png)
![p3](_doc/image/preview_3.png)
![p4](_doc/image/preview_4.png)
![p5](_doc/image/preview_5.png)
![p6](_doc/image/preview_6.png)

## 如何开始开发

请先安装好依赖的开发环境：Java8、Gradle、Node.js、vue-cli。我自己使用的是Gradle4.6，Node8.11.1，vue-cli 2.9.3，建议使用Intellij IDEA。




